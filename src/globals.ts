import { StackScreenProps } from '@react-navigation/stack'

type LearnAtom = {
    asset: string
    value: string
}

type RootStackParamList = {
    Login: undefined
    Home: undefined
    Learn: { name: string }
}

export type LoginProps = StackScreenProps<RootStackParamList, 'Login'>
export type MainProps = StackScreenProps<RootStackParamList, 'Home'>
export type LearnProps = StackScreenProps<RootStackParamList, 'Learn'>
