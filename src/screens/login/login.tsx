import React, { FC } from 'react';
import {
    Container,
    Content,
    Button,
    Text
} from 'native-base';
import { StyleSheet, Image } from 'react-native'
import { LoginProps } from "../../globals"

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffe100'
    },
    textContent: {
        fontSize: 45,
        color: '#ff7300',
        textAlign: 'center'
    },
    logo: {
        width: 200,
        height: 212,
        marginBottom: 40,
        marginTop: 70
    },
    button: {
        marginTop: 50,
        alignSelf: 'center'
    }
})

export const LoginPage: FC<LoginProps> = (props) => {

    return (
        <Container style={styles.container}>
            <Content
                contentContainerStyle={{
                    flex: 1,
                    alignItems: "center"
                }}
            >
                <Image
                    source={require("../../res/images/logo-bee.png")}
                    style={styles.logo}
                />
                <Text style={styles.textContent}>
                    Welcome to Signing Bee
                </Text>
                <Button
                    rounded
                    warning
                    style={styles.button}
                    onPress={() =>
                        props.navigation.navigate("Home")
                    }
                >
                    <Text>Lets get started</Text>
                </Button>
            </Content>
        </Container>
    )
}
