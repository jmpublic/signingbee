import React, { FC } from 'react';
import {
    Container,
    Content,
    Button,
    Text,
    Footer,
    FooterTab
} from 'native-base';
import { StyleSheet, Image, View } from 'react-native'
import { MainProps } from "../../globals"

const styles = StyleSheet.create({
    container: {
        flex: 0.5,
        height: "25%"
    },
    profile: {
        width: 300,
        height: 159,
        marginTop: 10,
        marginBottom: 5,
    },
    courses: {
        marginTop: 5
    },
    containerCourses: {
        flex: 1,
    },
    button: {
        marginTop: 50,
        alignSelf: 'center'
    }
})

export const MainPage: FC<MainProps> = ({ navigation }) => {
    return (
        <Container>
            <View
                style={styles.container}
            >
                <Content
                    contentContainerStyle={{
                        flex: 1
                    }}
                >
                    <Image
                        source={
                            require("../../res/images/test-profile.png")
                        }
                        style={styles.profile}
                    />
                </Content>
            </View>
            <Container style={styles.containerCourses}>
                <Content
                    contentContainerStyle={{
                        flex: 1,
                        alignItems: "center"
                    }}
                >
                    <Button
                        rounded
                        warning
                        style={styles.button}
                        onPress={() =>
                            navigation.navigate(
                                "Learn",
                                { name: 'Letters' }
                            )
                        }
                    >
                        <Text>Letters</Text>
                    </Button>
                </Content>
            </Container>
        </Container >
    )
}
