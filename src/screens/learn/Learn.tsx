import React, { FC, useState } from 'react';
import {
    Container,
    Content,
    Button,
    Text,
    Footer,
    FooterTab
} from 'native-base';
import { StyleSheet, Image, View, TouchableOpacity, ImageSourcePropType } from 'react-native'
import { LearnProps } from "../../globals"
import { RNCamera } from 'react-native-camera'

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    question: {
        flex: 1,
        alignItems: "center"
    },
    answerImage: {
        width: 50,
        height: 50,
        marginRight: 10
    },
    questionText: {
        fontSize: 30,
        marginBottom: 10
    },
    questionDisplay: {
        fontSize: 70,
        marginBottom: 40
    },
    preview: {
        flex: 1,
        justifyContent: 'flex-end',
        alignItems: 'center',
        height: "50%"
    }
})

const questions = {
    'A': {
        sign: require("../../res/letters/A.png"),
        correctValue: "A"
    },
    'B': {
        sign: require("../../res/letters/B.png"),
        correctValue: "B"
    },
    'C': {
        sign: require("../../res/letters/C.png"),
        correctValue: "C"
    },
    'D': {
        sign: require("../../res/letters/D.png"),
        correctValue: "D"
    }
}

const PendingView = () => (
    <View
        style={{
            flex: 1,
            backgroundColor: 'lightgreen',
            justifyContent: 'center',
            alignItems: 'center',
        }}
    >
        <Text>Waiting</Text>
    </View>
)

interface IAnswerButton {
    sign: { sign: ImageSourcePropType, correctValue: string }
    answer: string | null,
    userAnswer: null | string
    setUserAnswer: any
}

const AnswerButton: FC<IAnswerButton> = ({ sign, userAnswer, answer, setUserAnswer }) => {
    const c = userAnswer && sign.correctValue === answer ? "#41ff02" :
        userAnswer && sign.correctValue === userAnswer ? "#ff2f00" : '#dbd9d8'

    const handleClick = () => {
        setUserAnswer(sign.correctValue)
    }

    return (
        <TouchableOpacity
            activeOpacity={0.5}
            onPress={handleClick}
            style={{
                flex: 0,
                backgroundColor: c,
                borderRadius: 5,
                alignSelf: 'center',
            }}
        >
            <Image source={sign.sign} />
        </TouchableOpacity>
    )
}

interface INextButton {
    color: string,
    setAnswer: any,
    setUserAnswer: any,
    answer: string
}

const NextButton: FC<INextButton> = ({ color, answer, setAnswer, setUserAnswer }) => {
    const handlePress = () => {
        const na = Object.keys(questions).filter(function(item) {
            return item !== answer
        })
        setAnswer(randomL(na))
        setUserAnswer(null)
    }

    return (
        <Content>
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={handlePress}
                style={{
                    flex: 0,
                    backgroundColor: color,
                    borderRadius: 5,
                    alignSelf: 'center',
                    marginTop: 10,
                    padding: 5
                }}
            >
                <Text style={{ backgroundColor: color }}>Next</Text>
            </TouchableOpacity>
        </Content>
    )
}

interface IAnswerPanel {
    answer: string,
    setAnswer: any
}

const AnswerPanel: FC<IAnswerPanel> = ({ answer, setAnswer }) => {
    const [userAnswer, setUserAnswer] = useState(null)
    const c = userAnswer === answer ? "#41ff02" : "#ff2f00"

    return (
        <Content>
            <View style={styles.question}>
                <Text style={styles.questionText}>
                    What is the sign for:
                    </Text>
                <Text style={styles.questionDisplay}>{answer}</Text>
            </View>
            <Content
                contentContainerStyle={{
                    flex: 1,
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginLeft: 2,
                    marginRight: 2
                }}
            >
                <React.Fragment>
                    {Object.values(questions).map((x) => {
                        return <AnswerButton
                            sign={x}
                            answer={answer}
                            setUserAnswer={setUserAnswer}
                            userAnswer={userAnswer}
                            key={x.correctValue}
                        />
                    })}
                </React.Fragment>
            </Content>
            <Content>
                {userAnswer ?
                    <NextButton
                        color={c}
                        setAnswer={setAnswer}
                        answer={answer}
                        setUserAnswer={setUserAnswer}
                    /> : null}
            </Content>
        </Content>
    )
}

const randomL = (a: string[]) => {
    const r = Math.floor(Math.random() * a.length)
    return a[r]
}

export const LearnPage: FC<LearnProps> = ({ route }) => {
    const [answer, setAnswer] = useState(randomL(Object.keys(questions)))

    return (
        <Container>
            <AnswerPanel
                answer={answer}
                setAnswer={setAnswer}
            />
            <View style={styles.container}>
                <Content contentContainerStyle={{ flex: 1 }}>
                    <RNCamera
                        style={styles.preview}
                        type={RNCamera.Constants.Type.front}
                        flashMode={RNCamera.Constants.FlashMode.on}
                        androidCameraPermissionOptions={{
                            title: 'Permission to use camera',
                            message: 'We need your permission to use your camera',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                        androidRecordAudioPermissionOptions={{
                            title: 'Permission to use audio recording',
                            message: 'We need your permission to use your audio',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                    >
                        {({ camera, status, recordAudioPermissionStatus }) => {
                            if (status !== 'READY') return <PendingView />;
                            return (
                                <View style={{
                                    flex: 0,
                                    flexDirection: 'row',
                                    justifyContent: 'center'
                                }}>
                                </View>
                            );
                        }}
                    </RNCamera>
                </Content>
            </View>
        </Container >
    )
}
