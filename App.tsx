import 'react-native-gesture-handler'
import React from 'react'
import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { LoginPage } from "./src/screens/login/login"
import { MainPage } from "./src/screens/main/Main"
import { LearnPage } from "./src/screens/learn/Learn"

const Stack = createStackNavigator();

const App = () => {
    return (
        <NavigationContainer>
            <Stack.Navigator>
                <Stack.Screen name="Login" component={LoginPage} />
                <Stack.Screen name="Home" component={MainPage} />
                <Stack.Screen name="Learn" component={LearnPage} />
            </Stack.Navigator>
        </NavigationContainer>
    )
}

export default App;
